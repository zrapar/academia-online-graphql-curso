import { database } from './../data/data.store';
import { IResolvers } from 'graphql-tools';
import _ from 'lodash';

const type: IResolvers = {
	Estudiante: {
		courses: (parent) => {
			const listCourses: Array<any> = [];
			parent.courses.map((cursoId: string) => {
				listCourses.push(_.filter(database.cursos, [ 'id', cursoId ])[0]);
			});
			return listCourses;
		}
	},
	Curso: {
		students: (parent) => {
			const listStudents: Array<any> = [];
			database.estudiantes.map((estudiante: any) => {
				if (_.indexOf(estudiante.courses, parent.id) > 0) {
					listStudents.push(estudiante);
				}
			});
			return listStudents;
		},
		path: (parent) => `https://www.udemy.com${parent.path}`
	}
};

export default type;

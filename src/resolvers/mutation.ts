import { database } from './../data/data.store';
import { IResolvers } from 'graphql-tools';
import _ from 'lodash';

const mutation: IResolvers = {
	Mutation: {
		cursoNuevo(__: void, { curso }): any {
			const ItemCurso = {
				id: String(database.cursos.length + 1),
				title: curso.title,
				description: curso.description,
				clases: curso.clases,
				time: curso.time,
				logo: curso.logo,
				level: curso.level,
				path: curso.path,
				teacher: curso.teacher,
				reviews: []
			};

			if (
				database.cursos.filter(
					(itemCurs) => itemCurs.title.toLowerCase().trim() === ItemCurso.title.toLowerCase().trim()
				).length === 0
			) {
				database.cursos.push(ItemCurso);
				return ItemCurso;
			}

			return {
				id: '-1',
				title: `Ya existe un curso este titulo`,
				description: '',
				clases: -1,
				time: 0.0,
				logo: '',
				level: 'TODOS',
				path: '',
				teacher: '',
				reviews: []
			};
		},
		modificarCurso(__: void, { curso }): any {
			const elementoExiste = _.findIndex(database.cursos, (o) => {
				return o.id === curso.id;
			});

			if (elementoExiste > -1) {
				const valoraciones = database.cursos[elementoExiste].reviews;
				curso.reviews = valoraciones;
				database.cursos[elementoExiste] = curso;
				return curso;
			}
			return {
				id: '-1',
				title: `No existe el curso en la base de datos`,
				description: '',
				clases: -1,
				time: 0.0,
				logo: '',
				level: 'TODOS',
				path: '',
				teacher: '',
				reviews: []
			};
		},
		eliminarCurso(__: void, { id }): any {
			const deleteCourse = _.remove(database.cursos, (curso) => {
				return curso.id === id;
			});

			if (deleteCourse[0] === undefined) {
				return {
					id: '-1',
					title: `No existe el curso en la base de datos`,
					description: '',
					clases: -1,
					time: 0.0,
					logo: '',
					level: 'TODOS',
					path: '',
					teacher: '',
					reviews: []
				};
			}

			return deleteCourse[0];
		}
	}
};

export default mutation;

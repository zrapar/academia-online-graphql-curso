import express from 'express';
import compression from 'compression';
import cors from 'cors';
import { ApolloServer } from 'apollo-server-express';
import { createServer } from 'http';
import schema from './schema';
import expressPlayGround from 'graphql-playground-middleware-express';

const app = express();

app.use('*', cors());
app.use(compression());

app.get(
	'/',
	expressPlayGround({
		endpoint: '/graphql'
	})
);

const server = new ApolloServer({
	schema,
	introspection: true
});

server.applyMiddleware({ app });

const httpServer = createServer(app);
const PORT = 5200;

httpServer.listen({ port: PORT }, () => console.log(`Running in http://localhost:${PORT}`));
